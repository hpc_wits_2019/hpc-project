// USE //
// mpicc allreduce_ring_async.c genann.c -o allreduce_ring_async -lm
// mpiexec -n 8 ./allreduce_ring_async

#define USE_MNIST_LOADER
#define MNIST_DOUBLE
#include "mnist.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include "genann.h"
#include <time.h>
#include <mpi.h>

/* for argv convert to int */
#include <errno.h>   // for errno
#include <limits.h>  // for INT_MAX

double *input, *class; //data
unsigned int samples; //number of samples
const char *class_names[] = {"0","1","2","3","4","5","6","7","8","9"};


void load_mnist(char *images_fname, char *labels_fname)
{
    mnist_data *data_t, *temp;
    unsigned int cnt;
    int ret;

    if (ret = mnist_load(images_fname, labels_fname, &data_t, &cnt)) {
        printf("An error occured: %d\n", ret);
    } else {
        printf("image count: %d\n", cnt);
    }
    //cnt = 500; // was used for debugging with smaller data, to reduce run time
    /* Allocate memory for input and output data. */
    input = (double *) malloc(sizeof(double) * cnt * 28*28);
    if (input == NULL)
    {
        printf("Input malloc error");
        exit(-1);
    }
    class = (double *) malloc(sizeof(double) * cnt * 10);
    if (class == NULL)
    {
        printf("class malloc error");
        exit(-1);
    }


    temp = data_t;
    int i, j,k;
    for (i = 0; i <cnt; ++i) {
        double *p = input + i * 28*28;
        double *c = class + i * 10;
        c[0] = c[1] = c[2] = c[4] = c[5] = c[6] = c[7] = c[8] = c[9] = 0.0;
        //printf("pointers allocated for data row %d \n",i);
        for (j = 0; j < 28*28; ++j) {
               //printf("data line %d, j %d, image row %d,image col %d value = %f \n",i,j,j/28,j%28, temp->data[j/28][j%28]);
               *(p + j) = temp->data[j/28][j%28];
            }

        *(c + (int)temp->label) = 1.0;
        temp = temp + 1;
    }
    samples = cnt;
    //printf("image count %d", cnt);
    free(data_t);
}

int correct_predictions(genann *ann) {
    int correct = 0, j =0;
    for (j = 0; j < samples; ++j)
    {
        const double *guess = genann_run(ann, input + j*28*28);
        double max = 0.0;
        int k =0, actual =0, max_cls = 0;
        for (k =0; k < 10; k++)
        {
            if (guess[k]> max) {
                max = guess[k];
                max_cls = k;
            }
            if (class[j*10 + k]== 1.0) actual = k;
        }
//        printf(" predicted %d, actual %d \n",max_cls, actual);
        if (class[j*10 + (int)max_cls] == 1.0) ++correct;
        //else {printf("Logic error.\n"); exit(1);
    }
    return correct;
}


int main(int argc, char *argv[])
{
 /* Load the data from file to train */
    MPI_Init(&argc, &argv);
    int w_size, rank;
    MPI_Request request; MPI_Status status; int flag=0;//async

    MPI_Comm_size(MPI_COMM_WORLD, &w_size); // returns the number of processes, num_procs
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);

    if (rank == 0){
    printf("#######################\n");
    printf("AllReduce_Ring_Async ##\n");
    }

    //// convert argv to int for hyperparameter ////
    char *p;
    int h_param_1;
    errno = 0;
    long conv;
    if (argv[1] == NULL){
      if (rank==0){
        printf("please enter a hyperparameter as argv[1] - program will now exit\n");
        exit(0);
        }
      }
    else {
      conv = strtol(argv[1], &p, 10);
      /* Check for errors: e.g., the string does not represent an integer  or the integer is larger than int */
      if (errno != 0 || *p != '\0' || conv > INT_MAX) {// Put here the handling of the error, like exiting the program with an error message
        printf("argv was not an int");
        h_param_1 = -1; // this error handling does not work %%%
      } else { // No error
          h_param_1 = conv;
      }
    }
    /////////////////////////////////////

    if (rank == 0)
    {
      load_mnist("mnist/train-images-idx3-ubyte","mnist/train-labels-idx1-ubyte");
    }
      /* Initialize time elements */
      double ts, te;
      ts = MPI_Wtime();

      /*send number of samples to all nodes */
      MPI_Bcast(&samples, 1, MPI_UNSIGNED, 0, MPI_COMM_WORLD);

    /* scatter data to all the other nodes */
    int *count, *disp, *count_c, *disp_c,sum =0, sum_c = 0;
    count =(int *)  malloc(sizeof(int)*w_size); //to define limits of transfer total floats
    disp = (int *) malloc(sizeof(int)*w_size);
    count_c = (int *)  malloc(sizeof(int)*w_size); // % count for class
    disp_c = (int *) malloc(sizeof(int)*w_size);

    int s_size= samples/w_size;
    if (rank < samples%w_size) s_size++;

    for (int i = 0; i < w_size; i++) { //find the number of elems to send to each processor //% determine an array of displacements "disp" for scatter
        count[i] = samples/w_size*28*28;
        count_c[i] = samples/w_size*10;
        if (i < samples%w_size) {  count[i] += 28*28; count_c[i] +=10;  } //account for number of smaples not equally being divided by the number of processes
        disp[i] = sum; disp_c[i] = sum_c;
        sum += count[i]; sum_c +=count_c[i];
    }
    double *s_data, *s_class;
    s_data = (double *) malloc(sizeof(double) * count[rank]);
    s_class = (double *) malloc(sizeof(double) * count_c[rank]);

    MPI_Scatterv(input,count, disp, MPI_DOUBLE,s_data,count[rank],MPI_DOUBLE,0,MPI_COMM_WORLD); // %scatter 'input' ?equally? in s_data for count of ??
    MPI_Scatterv(class,count_c, disp_c, MPI_DOUBLE,s_class,count_c[rank],MPI_DOUBLE,0,MPI_COMM_WORLD);

      /*Initialize neural network */
    int h2 = h_param_1;
    genann *ann = genann_init(28*28, 3, h2, 10);// starts to diverge at about 120 - ring = 173 - server = 181



//////////////////////////////////////////////////
//////////// Where the magic happens ////////////
/////////////////////////////////////////////////
    /* Train the network with backpropagation. */
    int i, j;
    int epochs = 20;

    for (i = 0; i < epochs; ++i) {
        for (j = 0; j < s_size; ++j) {
            genann_train(ann, s_data + j*28*28,s_class + j*10, .1); // pointer arithmetic - equivalent to s_data[0], s_data[1]... s_data[j]
        }
//        printf("before reduce rank %d, ann->weight[20] : %f \n",rank,ann->weight[20]);
        MPI_Barrier(MPI_COMM_WORLD);

      //// allreduce_ring ////
      double *sum_buff;
      sum_buff = (double *) malloc(sizeof(double) * ann->total_weights);

      //start of ring - length of ring = w_size - therefore average by dividing by this size
      for (int k = 0; k<w_size; k++)
      {
        /* sum the initial and then recieved parameters(weights) into the sum buffer - ?then average? */
        for (int l = 0; l< ann->total_weights; l++)
        {
          sum_buff[l] = sum_buff[l] + ann->weight[l] ;
        }
        //recieve from left, send to right
        MPI_Isend(ann->weight,1, MPI_INT, (rank+1)%w_size, 0, MPI_COMM_WORLD,&request);
        MPI_Recv(ann->weight,1, MPI_INT, (rank-1)%w_size, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
        MPI_Test(&request, &flag, &status);
      }
        //MPI_Allreduce(MPI_IN_PLACE,ann->weight,ann->total_weights,MPI_DOUBLE,MPI_SUM,MPI_COMM_WORLD); //arrow operator "pointer_to_struct -> member" is equivalent to "(*pointer_to_struct).member"  https://www.youtube.com/watch?v=J2iHTv5VVgg https://en.wikipedia.org/wiki/Operators_in_C_and_C%2B%2B#Member_and_pointer_operators
        /////////////////////////

      for (j=0;j<ann->total_weights;j++) { ann->weight[j] = sum_buff[j]/w_size; } //% average by division of number of proccess
      MPI_Barrier(MPI_COMM_WORLD);

    }
///////////////////////////////////////////////////
//////////////////////////////////////////////////

    te = MPI_Wtime();
    double cpu_time_used = (double) (te - ts);
    if (rank == 0) { printf("train time taken : %f \n",cpu_time_used);}

    free(input);
    free(class);

    if (rank == 0)
    {
    /* Load data from file to test */
    load_mnist("mnist/t10k-images-idx3-ubyte","mnist/t10k-labels-idx1-ubyte");

    /* find accuracy */
    int correct = correct_predictions(ann);
    double accuracy = (double)correct / samples * 100.0;
    printf("\n\n %d/%d correct (%0.1f%%).\n", correct, samples, accuracy);

    /* write results to file*/
    FILE * fp;
    fp = fopen ("results.csv","a");
    /* method, weight_matrix_size, elapse_time, accuracy, number_of_nodes */
    fprintf (fp , "allreduce_ring_async, %d, %0.2f% ,%0.1f%, %d, \n",ann->total_weights, cpu_time_used,accuracy, w_size);
    fclose (fp);

    }
    MPI_Finalize();
    free(s_data);
    free(s_class);
    genann_free(ann);

    printf("\n");


    return 0;
}
