# HPC Project 2019 - Distributed Deep Learning


Reports and presentation slides can be found in the "Docs" folder.
</n>

Code for both projects can be found in the "Code" folder
</n>

## Dependencies 

Compiling C code  
* `sudo apt install make`
* `sudo apt install gcc`

CUDA
* NVidia Graphics Card 
* [install CUDA](https://www.pugetsystems.com/labs/hpc/How-to-install-CUDA-9-2-on-Ubuntu-18-04-1184/) 

MPI 
* Intall MPI `sudo apt-get install libcr-dev mpich2 mpich2-doc`
* [Additional info](https://gist.github.com/pajayrao/166bbeaf029012701f790b6943b31bb2)

## Distributed Deep Learning

#### MPI and Serial

Raw results for experiments can be found in the .csv file labeled "results.csv"
</n>
To run on the cluster simply run

`sbatch job_script.slurm`

For more granular testing one can run `make`

and then run each file separately or together with `./run.sh`

The serial program is run with `./run_serial.sh`

The run.sh contains the code to vary the programs hyperparameters

#### CUDA and MPI
While the the CUDA & MPI Heterogeneous Parallel implementation successfully compiled and ran with all of the CUDA code blocks producing reasonable outputs, it unfortunately did not successfully train. The cause of this has not yet been elucidated, however it seems to be a small error as the vast majority of the code is correct. The error is likely to be caused by the way the Gennan library stores its neural net struct in a single contiguous memory array. It is likely that one of the structure modules is not being updated correctly. At the time of submission this experiment will still be running 2 full iterations on the cluster, as to comprehensively be able to state that every variant of the program is runnable on the cluster.


## PyCUDA ##
</n>
The code for the Colab notebook is available as both a .ipynb and .py file. The link provided in the report to the online Colab version is also available and will not be altered after submission. This can be cross checked with the submitted code.

Here too there was a bug with one of the user defined classes that caused the PyCuda kernel grid size to be set incorrectly / stuck on a size of (1,1,1), so that only small arrays could be passed into the kernel.



